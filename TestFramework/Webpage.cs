﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.SupportClasses;

namespace TestFramework
{
    public static class Webpage
    {
        public static IWebDriver webDriver;
        public static Actions actions;

        public static Header Header
        {
            get
            {
                var header = new Header(Browser.webDriver, Browser.actions );
                return header;
            }
        }

        public static Footer Footer
        {
            get
            {
                var footer = new Footer(Browser.webDriver, Browser.actions);
                return footer;
            }
        }

        public static Main Main
        {
            get
            {
                var main = new Main(Browser.webDriver, Browser.actions);
                return main;
            }
        }

        public static bool IsMiniCartDisplayed()
        {
            if (Browser.webDriver.FindElement(By.ClassName("c-mini-cart")).Displayed)
                return true;
            else
                return false;
        }

        public static bool IsAtDomain(string siteUrl)
        {
            return Browser.GetUrl().StartsWith(siteUrl);
        }

        public static void ClearAllCookies()
        {
            Browser.ClearAllCookies();
        }

        public static string GetUrl()
        {
            return Browser.GetUrl();
        }

        public static void OpenBrowser()
        {
            Browser.NewBrowserWindow();
        }

        public static void OpenBrowser(string browsertype)
        {
            if(browsertype == "chrome") { Browser.NewChromeWindow(); }
            else if (browsertype == "firefox") { Browser.NewFirefoxWindow(); }
            else if (browsertype == "edge") { Browser.NewEdgeWindow(); }
            else if (browsertype == "ie11") { Browser.NewEI11Window(); }
            else { OpenBrowser(); }

        }

        public static void GoTo(string url)
        {
            Browser.Goto(url);
        }

        public static bool IsAt(string url)
        {
           return Browser.Title.Contains(url);
        }

        public static void Quit()
        {
            Browser.Quit();
        }

        public static void ClickLink(string url)
        {
            Browser.SelectLink(url);
            Webpage.WaitUntilPageIsLoaded();
        }

        public static void WaitUntilPageIsLoaded()
        {
            Thread.Sleep(2500);
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            new WebDriverWait(Browser.webDriver, TimeSpan.FromSeconds(30)).Until(
                d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public static void AddTextInForm(IWebElement inputForm, string input)
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)webDriver;
            jse.ExecuteScript("arguments[0].setAttribute('value', '" + input + "');", inputForm);
        }

        public static void ClickElement(string findMethod, string findString)
        {
            if (findMethod == "ClassName") { Browser.webDriver.FindElement(By.ClassName(findString)).Click(); }
            else if (findMethod == "TagName") { Browser.webDriver.FindElement(By.TagName(findString)).Click(); }
            else { throw new Exception("Unknown findmethod"); }
        }

        public static void WaitUntilElementIsVisable(IWebElement webElement)
        {
            new WebDriverWait(Browser.webDriver, TimeSpan.FromSeconds(15)).Until(
                e => webElement.Displayed);
        }

        public static bool DoesElementExist(IWebElement parent, By selectionMethod)
        {
            try
            {
                parent.FindElement(selectionMethod);
                return true;
            }
            catch { return false; }
        }

        public static void ScrollToElement(IWebElement element)
        {
            //String below found at https://stackoverflow.com/questions/20167544/selenium-scroll-element-into-center-of-view
            String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                                            + "var elementTop = arguments[0].getBoundingClientRect().top;"
                                            + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

            IJavaScriptExecutor jse = (IJavaScriptExecutor)Browser.webDriver;
            jse.ExecuteScript(scrollElementIntoMiddle, element);
        }

        public static IWebElement GetWebElement(string byString)
        {
            By by = StringToByParser.LocatorParser(byString);

            return Browser.webDriver.FindElement(by);
        }

        public static void CheckIfPageIsReady()
        {

            IJavaScriptExecutor js = (IJavaScriptExecutor)Browser.webDriver;


            //Initially bellow given if condition will check ready state of page.
            if (js.ExecuteScript("return document.readyState").ToString().Equals("complete"))
            {
                Console.Out.Write("Page Is loaded.");
                return;
            }

            //This loop will rotate for 25 times to check If page Is ready after every 1 second.
            //You can replace your value with 25 If you wants to Increase or decrease wait time.
            for (int i = 0; i < 25; i++)
            {
                try
                {
                    Thread.Sleep(1000);
                }
                catch (ThreadInterruptedException) { }
                //To check page ready state.
                if (js.ExecuteScript("return document.readyState").ToString().Equals("complete"))
                {
                    break;
                }
            }
        }
    }

}
