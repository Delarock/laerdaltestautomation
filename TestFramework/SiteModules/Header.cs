﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using TestFramework.SupportClasses;

namespace TestFramework
{
    public class Header
    {
        private IWebDriver webDriver;
        private Actions actions;
        private IWebElement header;

        public Header(IWebDriver webDriver, Actions actions)
        {
            this.webDriver = webDriver;
            this.actions = actions;
        }

        public List<HeaderModule> GetHeaderLinks()
        {
            var headerModules = new List<HeaderModule>();
            var HeaderObjects = webDriver.FindElement(By.ClassName("c-header")).FindElements(By.TagName("li"));

            foreach (var e in HeaderObjects)
            {
                headerModules.Add(new HeaderModule
                {
                    headerModuleText = e.FindElement(By.TagName("a")).GetAttribute("textContent"),
                    headerModuleLink = e.FindElement(By.TagName("a")).GetAttribute(("href")),
                    headerModuleClass = e.FindElement(By.TagName("a")).GetType().ToString(),
                    headerModuleXpath = e.FindElement(By.TagName("a")).GetAttribute("innerHTML")
                });
            }
            return headerModules;
        }

        public void ClickMyAccount()
        {
            actions = new Actions(webDriver);
            actions.MoveToElement(webDriver.FindElement(By.ClassName("e-header-tools__link--myaccount"))).Click().Perform();
            Webpage.WaitUntilPageIsLoaded();
        }

        public void OpenCartFromMiniCart()
        {
            actions = new Actions(webDriver);
            actions.MoveToElement(webDriver.FindElement(By.ClassName("c-mini-cart")).FindElement(By.ClassName("e-button"))).Click().Perform();
            Webpage.WaitUntilPageIsLoaded();
        }

        public void OpenMiniCart()
        {
            IWebElement miniCartButton = webDriver.FindElement(By.ClassName("e-header-tools__link--cart"));
            Webpage.ScrollToElement(miniCartButton);
            actions = new Actions(webDriver);
            actions.MoveToElement(miniCartButton).Click().Perform();
            Webpage.WaitUntilPageIsLoaded();
        }

        public void OpenCart()
        {
            IWebElement miniCartButton = webDriver.FindElement(By.ClassName("e-header-tools__link--cart"));
            Webpage.ScrollToElement(miniCartButton);
            actions = new Actions(webDriver);
            actions.MoveToElement(miniCartButton).Click().Perform();
            Webpage.WaitUntilPageIsLoaded();
            
        if (GetCartItemCount() != 0)
        {
            OpenCartFromMiniCart();
        }
        }

        public int GetCartItemCount()
        {
            var itemCountString = webDriver.FindElement(By.ClassName("e-header-tools__cart-item-count")).Text;
            itemCountString = itemCountString.Substring(1, itemCountString.Length - 2);
            return Int32.Parse(itemCountString);
        }

        public void SearchFor(string v)
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            var headerSearch = webDriver.FindElement(By.ClassName("e-header-tools__link--search"));
            actions = new Actions(webDriver);
            actions.MoveToElement(headerSearch).Click().Perform();
            var searchPopup = webDriver.FindElement(By.ClassName("e-header-search"));
            var searchField = searchPopup.FindElement(By.ClassName("e-header-search__input"));
            var searchSubmit = searchPopup.FindElement(By.ClassName("e-header-search__submit"));
            actions.MoveToElement(searchField).SendKeys("Anne").Perform();
            actions.MoveToElement(searchSubmit).Click().Perform();
            Webpage.WaitUntilPageIsLoaded();

        }

        public List<string> GetHeaderHeaders()
        {
            List<string> headerTextList = new List<string>();
            var navLevelOne = webDriver.FindElements(By.XPath("//*[@data-nav-level = '1']"));
            foreach (var nav in navLevelOne)
            {
                headerTextList.Add(nav.Text);
            }
            headerTextList.RemoveAt(headerTextList.Count - 1);

            return headerTextList;
        }

        public void GoToHeaderLevelOne(int headerNumber)
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            var navLevelOne = webDriver.FindElements(By.XPath("//*[@data-nav-level = '1']"));
            navLevelOne[headerNumber].Click();
            Webpage.WaitUntilPageIsLoaded();
        }

        public void GoToHeaderLevelTwo(int headerNumber, int subHeaderNumber)
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            var navLevelOne = webDriver.FindElements(By.XPath("//*[@data-nav-level = '1']"));
            var levelOneHeader = navLevelOne[headerNumber];
            var navLevelTwo = levelOneHeader.FindElements(By.XPath(".//*[@data-nav-level = '2']"));
            var navLevelTwoCorrect = new Collection<IWebElement>();
            var eX = new Collection<IWebElement>();

            //Try For each!!!!
            foreach (IWebElement e in navLevelTwo)
            {
                navLevelTwoCorrect.Add(e.FindElement(By.TagName("a")));
                eX.Add(e.FindElement(By.XPath("./*[1]")));
            } 

            var headerToNav = navLevelTwoCorrect[subHeaderNumber];
            string tempUrl;
            try { tempUrl = eX[subHeaderNumber].GetAttribute("href"); }
            catch { tempUrl = "Not found"; }

            actions.MoveToElement(levelOneHeader).Click().Perform();
            Webpage.WaitUntilElementIsVisable(eX[subHeaderNumber]);
            actions.MoveToElement(eX[subHeaderNumber]).Click().Perform();

            Webpage.WaitUntilPageIsLoaded();
            Webpage.CheckIfPageIsReady();

            //Verify that we are at the correct site
            if (Browser.GetUrl() != tempUrl)
            {
                throw new Exception("Could not correctly go to the url " + tempUrl + " . ");
            }
        }
        public void GoToHeaderLevelThree(int headerNumber, int subHeaderNumber, int subZeroHeaderNumber)
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            var navLevelOnes = webDriver.FindElements(By.XPath("//*[@data-nav-level = '1']"));
            var navLevelOne = navLevelOnes[headerNumber];
            var navLevelTwos = navLevelOne.FindElements(By.XPath(".//*[@data-nav-level = '2']"));
            var navLevelTwo = navLevelTwos[subHeaderNumber];
            var navLevelThrees = navLevelOne.FindElements(By.XPath(".//*[@data-nav-level = '3']"));
            var navLevelThree = navLevelThrees[subZeroHeaderNumber].FindElement(By.TagName("a"));

            

            actions.MoveToElement(navLevelOne).Click().Perform();
            Webpage.WaitUntilElementIsVisable(navLevelThree);
            actions.MoveToElement(navLevelThree).Click().Perform();

            //Get targetUrl
            string targetUrl;
            try { targetUrl = navLevelThree.GetAttribute("href"); }
            catch { targetUrl = "Not found"; }


            Webpage.WaitUntilPageIsLoaded();
            Webpage.CheckIfPageIsReady();

            //Verify that we are at the correct site
            if (Browser.GetUrl() != targetUrl)
            {
                throw new Exception("Could not correctly go to the url " + targetUrl + " . ");
            }
        }

        public bool FindMyAccount()
        {
            var headerTools = webDriver.FindElement(By.ClassName("e-header-tools__list"));
            try
            {
                var headerMyAccount = headerTools.FindElement(By.ClassName("e-header-tools__link--myaccount"));
                return headerMyAccount.Displayed;
                //return true;
            }
            catch (Exception e) when (e is NoSuchElementException || e is ArgumentOutOfRangeException)
            {
                return false;
            }
        }

        public bool FindSearch()
        {
            var headerTools = webDriver.FindElement(By.ClassName("e-header-tools__list"));
            try
            {
                var headerSearch = headerTools.FindElement(By.ClassName("e-header-tools__link--search"));
                return headerSearch.Displayed;
                //return true;
            }
            catch (Exception e) when (e is NoSuchElementException || e is ArgumentOutOfRangeException)
            {
                return false;
            }
        }

        public bool FindCart()
        {
            var headerTools = webDriver.FindElement(By.ClassName("e-header-tools__list"));
            try
            {
                var headerCart = headerTools.FindElement(By.ClassName("e-header-tools__link--cart"));
                return headerCart.Displayed;
                //return true;
            }
            catch (Exception e) when (e is NoSuchElementException || e is ArgumentOutOfRangeException)
            {
                return false;
            }
        }
    }
}