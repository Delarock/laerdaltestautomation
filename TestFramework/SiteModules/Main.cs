﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TestFramework.SupportClasses;

namespace TestFramework
{
    public class Main
    {
        private IWebDriver webDriver;
        private Actions actions;
        private IWebElement main;

        public Main(IWebDriver webDriver, Actions actions)
        {
            this.webDriver = webDriver;
            this.actions = actions;
        }

        public BlockResultAndString VerifyBlocks()
        {
            

            string statusString;
            main = webDriver.FindElement(By.Id("content"));

            //Check if there is a expandation option on the page, and if so toggle it
            try
            {
                if (main.FindElement(By.ClassName("e-view-more__link")).Displayed)
                {
                    Browser.ScrollToElement(main.FindElement(By.ClassName("e-view-more__link")));
                    main.FindElement(By.ClassName("e-view-more__link")).Click();
                }
            } catch
            {
                //Do nothing
            }

            //This should identify the element of all blocks on page
            var mainModuleList = main.FindElement(By.ClassName("content-area")).FindElements(By.XPath("./child::*"));

            var ListOfBlocksWithClass = new List<BlockAndClass>();

            //This should identify the block class for each block element
            foreach (IWebElement e in mainModuleList)
            {
                ListOfBlocksWithClass.Add(new BlockAndClass { element = e, blockType = e.GetAttribute("class") });
            }

            //Do some sort of validation or check for each individual block
            foreach (BlockAndClass bac in ListOfBlocksWithClass)
            {
                statusString = currentBlock(bac);
                try {
                    if (bac.blockType == "block herocarouselblock") {
                        var tempString = BlockValidators.ValidateBlock_HeroCarousel(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else if (bac.blockType.Contains("block containerblock")) {
                        var tempString = BlockValidators.ValidateBlock_Container(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    } //containerblocktwelvecolumns?
                    else if (bac.blockType.Contains("block standardpage")) //large?
                    {
                        var tempString = BlockValidators.ValidateBlock_StandardPage(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else if (bac.blockType == "block featuredcontentblock") {
                        var tempString = BlockValidators.ValidateBlock_FeaturedContent(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else if (bac.blockType == "block whatsnewcontainerblock")
                    {
                        var tempString = BlockValidators.ValidateBlock_WhatsNewContainer(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else if (bac.blockType == "block ctablock")
                    {
                        var tempString = BlockValidators.ValidateBlock_CTA(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else if (bac.blockType == "block wrappingblock")
                    {
                        var tempString = BlockValidators.ValidateBlock_Wrapping(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else if (bac.blockType.Contains("block simpleeditorialblock"))
                    {
                        var tempString = BlockValidators.ValidateBlock_SimpleEditorial(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else if (bac.blockType == "block stickynavigationitemblock")
                    {
                        //Do nothing for now.
                    }
                    else if (bac.blockType == "block imagefile")
                    {
                        var tempString = BlockValidators.ValidateBlock_Imagefile(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else if (bac.blockType == "block testimonialblock")
                    {
                        var tempString = BlockValidators.ValidateBlock_Testimonial(bac.element);
                        if (tempString != "") { statusString += tempString; throw new Exception(); }
                    }
                    else { return new BlockResultAndString { testStatus = false, blockInfo = "No test found for block type: " + statusString }; }
                }
                catch (Exception ex) {
                    statusString += " Exception details: " + ex.ToString();
                    return new BlockResultAndString { testStatus = false, blockInfo = statusString };
                }
            }


            return new BlockResultAndString { testStatus = true, blockInfo = "Alles in Ordnung" };

            string currentBlock(BlockAndClass bac) {
                return "Test failed on block class '" + bac.blockType + "'. ";
            }
        }

        public void SignInWith(string v1, string v2)
        {
            throw new NotImplementedException();
        }

        public void AddItemViaEasyOrderEntry(string variantId, string quantity)
        {
            if(!Browser.GetUrl().EndsWith("cart/"))
            {
                Webpage.Header.OpenMiniCart();
                Webpage.ClickLink("View cart & checkout");
            }

            webDriver.FindElement(By.ClassName("c-easy-order__input")).SendKeys(variantId);
            webDriver.FindElement(By.ClassName("c-easy-order__input-qty")).SendKeys(quantity);
            webDriver.FindElement(By.ClassName("c-easy-order")).FindElement(By.ClassName("e-button")).Click();
            Webpage.WaitUntilPageIsLoaded();

        }

        public Collection<IWebElement> GetVariantBlocks_InStock()
        {
            var allVariantBlocks = webDriver.FindElements(By.ClassName("c-variant-tile"));
            var inStockBlocks = new Collection<IWebElement>();

            foreach (IWebElement block in allVariantBlocks)
            {
                try
                {
                    if (block.FindElement(By.ClassName("c-variant-tile__stock")).Displayed)
                    {
                        inStockBlocks.Add(block);
                    }
                }
                catch { }
            }

            return inStockBlocks;
        }

        public void EmptyCart()
        {
            if (!webDriver.FindElement(By.ClassName("c-mini-cart")).Displayed)
            {
                Webpage.Header.OpenMiniCart();
            }

            //Click "Remove" on items as long as there are items to find. When there are no items left, a NoSuchElementException will be thrown.
            try
            {
                while (webDriver.FindElement(By.ClassName("c-line-items__remove")).Displayed)
                {
                    var removeButton = webDriver.FindElement(By.ClassName("c-line-items__remove"));
                    removeButton.Click();
                    Webpage.WaitUntilPageIsLoaded();
                }
            }
            catch { }
        }

        public void ClickProduct(string productTitle)
        {
            var product = webDriver.FindElement(By.ClassName("content-area")).FindElement(SupportClasses.Selectors.SelectorByTagAndAttributeValue("a", "title", productTitle)).FindElement(By.XPath(".."));

            try { Webpage.ScrollToElement(product); } catch { }
            var productParent = product.FindElement(By.XPath(".."));
            var productGrandParent = product.FindElement(By.XPath("../.."));
            //var productCousin = product.FindElement(By.XPath("../div/"));
            //var productCousinChild = product.FindElement(By.XPath("../div/a/"));

            product.Click();
            Webpage.WaitUntilPageIsLoaded();
        }

        public void AddFirstAvailableItemToCart()
        {
            actions = new Actions(webDriver);
            var addToCartButton = webDriver.FindElement(By.ClassName("c-variant-tile__btn"));
            Webpage.ScrollToElement(addToCartButton);
            actions.MoveToElement(addToCartButton).Click().Perform();
            Webpage.WaitUntilPageIsLoaded();
        }

        public void ClickShopButton()
        {
            var shopButton = webDriver.FindElement(By.LinkText("Shop"));
            shopButton.Click();
            Webpage.WaitUntilPageIsLoaded();
        }

        public void QuickAddItemToCart(string variantId)
        {
            Webpage.GoTo("https://p2uat.laerdal.com/cart/");

        }

        public Boolean VerifyThatVariantsHavePrices()
        {
            Collection<IWebElement> variantBlocks = Webpage.Main.GetVariantBlocks_InStock();

            foreach (IWebElement vb in variantBlocks)
            {
                try
                {
                    bool isPriceDisplayed = !vb.FindElement(By.ClassName("c-variant-tile__price")).Displayed;
                    //If the price element is not displayed, return false
                    if (!isPriceDisplayed) { return false; }
                }
                catch
                {
                    //If the price element is not found, return false
                    return false;
                } ;
            }
            //else, return true
            return true;
        }
    }

}