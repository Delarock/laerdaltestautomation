﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;

namespace TestFramework
{
    public class Footer
    {
        private IWebDriver webDriver;
        private Actions actions;
        private IWebElement footer;

        public Footer(IWebDriver webDriver, Actions actions)
        {
            this.webDriver = webDriver;
            this.actions = actions;
        }

        public void GoTo_IntPageViaFooter()
        {
            footer = webDriver.FindElement(By.ClassName("c-footer"));
            var changeCountryButton = footer.FindElement(By.ClassName("c-footer__toggle-country-selector"));

            Browser.ScrollToElement(changeCountryButton);

            actions.MoveToElement(footer).Perform();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);

            actions.MoveToElement(changeCountryButton).Perform();
            changeCountryButton.Click();

            var countryPageLink = footer.FindElement(By.XPath("//*[@href='/']"));
            countryPageLink.Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
        }

        public List<string> GetFooterHeaders()
        {
            List<string> headerTextList = new List<string>();
            var footerHeadersH2 = webDriver.FindElement(By.ClassName("c-footer")).FindElements(By.TagName("H2"));
            foreach (var nav in footerHeadersH2)
            {
                headerTextList.Add(nav.Text);
            }
            return headerTextList;
        }

        public void GoToFooterHeaderLevelOne(int headerNumber)
        {
            var footerHeadersH2 = webDriver.FindElement(By.ClassName("c-footer")).FindElements(By.TagName("H2"));
            Browser.ScrollToElement(footerHeadersH2[0]);
            footerHeadersH2[headerNumber].FindElement(By.TagName("a")).Click();
            Webpage.WaitUntilPageIsLoaded();
        }

        public void GoToFooterHeaderLevelTwo(int headerNumber, int subHeaderNumber)
        {
            throw new NotImplementedException();
        }

        public bool IsFormEmpty()
        {
            return string.IsNullOrEmpty(webDriver.FindElement(By.Id("ac82dc6f-b80e-409a-9299-dd22d8ef39c5")).GetAttribute("value"));
        }

        public void ResetForm()
        {
            VerifyCorrectPage();
            var resetButton = webDriver.FindElement(By.XPath("//*[@type='reset']"));
            Browser.ScrollToElement(resetButton);
            resetButton.Click();

        }

        private void MoveDownUntilVisible(IWebElement element)
        {
            
            while (element.Displayed)
            {
                webDriver.FindElement(By.CssSelector("body")).SendKeys(Keys.ArrowDown);
            }
        }

        public void UpdateNewsletterProfile()
        {
            VerifyCorrectPage();
            var submitButton = webDriver.FindElement(By.Id("be66a9ce-2db0-484a-8e44-f5b73cd960b7"));
            Browser.ScrollToElement(submitButton);
            submitButton.Click();
            System.Threading.Thread.Sleep(1000); // To ensure that page view changes/loads.
        }

        public void Unsubscribe()
        {
            VerifyCorrectPage();
            IList<IWebElement> sunscriptionRadioBox = webDriver.FindElement(By.ClassName("FormChoice__Items")).FindElements(By.TagName("label"));
            Browser.ScrollToElement(sunscriptionRadioBox[1]); //The second option is unsubscribe
            sunscriptionRadioBox[1].Click();
        }

        public void FillInNewsletterInformation()
        {
            Random rnd = new Random();

            VerifyCorrectPage();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            //Fill in firstname
            webDriver.FindElement(By.Id("ac82dc6f-b80e-409a-9299-dd22d8ef39c5")).SendKeys("Tester");
            //Fill in lastname
            webDriver.FindElement(By.Id("6aefc782-ee88-4759-a22e-0d42c9ba9915")).SendKeys("McTesting");
            //Fill in phone number
            webDriver.FindElement(By.Id("ece54e55-ded9-477a-9c1e-766a93e4a6f1")).SendKeys("123456789");

            //Fill random market if present
            try
            {
                var marketDropdown = webDriver.FindElement(By.Id("f9b88d1e-67ea-4709-b270-052353334c4b"));
                IList<IWebElement> ddMarketOptions = marketDropdown.FindElements(By.TagName("option"));
                marketDropdown.SendKeys(ddMarketOptions[rnd.Next(0, ddMarketOptions.Count)].Text);
            } catch (NoSuchElementException) { }

            //Fill random state if present (Brazil)
            try
            {
                var stateDropdown = webDriver.FindElement(By.Id("5b98f074-8866-4223-8b76-be0f77c742b9"));
                IList<IWebElement> ddStateOptions = stateDropdown.FindElements(By.TagName("option"));
                stateDropdown.SendKeys(ddStateOptions[rnd.Next(0, ddStateOptions.Count)].Text);
            } catch (NoSuchElementException) { }

            //Fill job title
            webDriver.FindElement(By.Id("887b2ed8-af6b-4bc3-bd8c-87e141f53da4")).SendKeys("Tester");

            //Fill random job role
            var jobDropdown = webDriver.FindElement(By.Id("abc3d63b-f401-408d-87d2-485b8597e471"));
            IList<IWebElement> ddJobOptions = jobDropdown.FindElements(By.TagName("option"));
            jobDropdown.SendKeys(ddJobOptions[rnd.Next(0, ddJobOptions.Count)].Text);

            //Fill all checkboxes (this currently does not work)
            IList<IWebElement> checkboxInterest = webDriver.FindElements(By.XPath("//input[@type='checkbox']"));
            foreach (IWebElement e in checkboxInterest)
            {
                Browser.ScrollToElement(e);
                e.FindElement(By.XPath("//*")).Click();
            }
        }

        public bool ReceiveSignupConfirmation()
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            webDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(2);

            var statusAreaElement = webDriver.FindElement(By.XPath("//div[@class='Form__Status']"));
            string statusText = statusAreaElement.Text;
            
            return !string.IsNullOrWhiteSpace(statusText) &&
                statusText == "Thank you for subscribing." ||
                statusText == "Submit successful.";

            //if (statusText == "Thank you for subscribing" || statusText == "Submit successful.")
            //{
            //    return true;
            //}
            //else return false;
        }

        public void InitiateSignupToNewsletter()
        {
            footer = webDriver.FindElement(By.ClassName("c-footer"));
            Browser.ScrollToElement(footer);
            var newsletterInput = webDriver.FindElement(By.Id("newsletteremail"));
            newsletterInput.SendKeys("testing.tester@fake.com");
            newsletterInput.Submit();
        }

        public bool FindCopyright()
        {
            footer = webDriver.FindElement(By.ClassName("c-footer"));
            return footer.FindElement(By.ClassName("c-footer__copyright")).Displayed;

        }

        public void GoTo_CountryPageViaFooter(string CountryTag)
        {

            footer = webDriver.FindElement(By.ClassName("c-footer"));
            var changeCountryButton = footer.FindElement(By.ClassName("c-footer__toggle-country-selector"));

            Browser.ScrollToElement(footer);

            actions.MoveToElement(footer).Perform();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);

            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
            actions.MoveToElement(changeCountryButton).Perform();
            changeCountryButton.Click();

            var intPageLink = footer.FindElement(By.XPath("//*[@href='/" + CountryTag + "/']"));
            //actions.MoveToElement(intPageLink).Perform();
            intPageLink.Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
        }

        public void VerifyCorrectPage()
        {
            //Check that you are on the correct page
            try { webDriver.FindElement(By.ClassName("sr-only")); }
            catch { new Exception("On wrong page"); }
        }
    }
}