﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Edge;
using System.Linq;

namespace TestFramework
{
    public static class Browser
    {
        public static IWebDriver webDriver;
        public static Actions actions;

        public static void NewBrowserWindow()
        {
            webDriver = new FirefoxDriver(@"C:\Users\mhovdan\source\repos\TestFramework\TestFramework\bin\Debug\netstandard2.0");
            actions = new Actions(webDriver);
            webDriver.Manage().Window.Maximize();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }

        internal static void NewChromeWindow()
        {
            webDriver = new ChromeDriver(@"C:\Users\mhovdan\source\repos\TestFramework\TestFramework\bin\Debug\netstandard2.0");
            actions = new Actions(webDriver);
            webDriver.Manage().Window.Maximize();
        }

        internal static void NewFirefoxWindow()
        {
            webDriver = new FirefoxDriver(@"C:\Users\mhovdan\source\repos\TestFramework\TestFramework\bin\Debug\netstandard2.0");
            actions = new Actions(webDriver);
            webDriver.Manage().Window.Maximize();
        }

        internal static void NewEI11Window()
        {
            webDriver = new InternetExplorerDriver(@"C:\Users\mhovdan\source\repos\TestFramework\TestFramework\bin\Debug\netstandard2.0");
            actions = new Actions(webDriver);
            webDriver.Manage().Window.Maximize();
        }

        internal static void NewEdgeWindow()
        {
            webDriver = new EdgeDriver(@"C:\Users\mhovdan\source\repos\TestFramework\TestFramework\bin\Debug\netstandard2.0");
            actions = new Actions(webDriver);
            webDriver.Manage().Window.Maximize();
        }

        public static void Goto(string url)
        {

            webDriver.Url = url;
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
        }

        public static string Title
        {
            get { return webDriver.Title; }
        }

        public static void Close()
        {
            webDriver.Close();

        }

        public static void Quit()
        {
            webDriver.Quit();

        }

        internal static void ClearAllCookies()
        {
            webDriver.Manage().Cookies.DeleteAllCookies();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
        }

        internal static void SwitchToLastTab()
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            webDriver.SwitchTo().Window(webDriver.WindowHandles.Last());

        }

        internal static void SwitchToFirstTab()
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            webDriver.SwitchTo().Window(webDriver.WindowHandles.First());
        }

        internal static string GetUrl()
        {
            return webDriver.Url;

        }

        internal static void SelectLink(string s)
        {
            webDriver.FindElement(By.LinkText(s)).Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
        }

        public static void ScrollToElement(IWebElement element)
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)webDriver;
            jse.ExecuteScript("arguments[0].scrollIntoView();", element);
        }

    }
}
