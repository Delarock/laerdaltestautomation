﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFramework.SupportClasses
{
    class StringToByParser
    {
        public static By LocatorParser(String locator)
        {

            By loc = By.Id(locator);

            if (locator.Contains("Class")) 
            loc = By.ClassName(locator.Substring(10, locator.Length - 12)); 

            else if (locator.Contains("Id"))
                loc = By.Id(locator.Substring(7, locator.Length - 9));

            else if (locator.Contains("Name"))
                loc = By.Name(locator.Substring(9, locator.Length - 11));

            else if (locator.Contains("XPath"))
                loc = By.XPath(locator.Substring(10, locator.Length - 12));

            return loc;

        }
    }
}
