﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework;

namespace TestFramework.SupportClasses
{
    //Class used to simplify validation of individual blocks
    class BlockValidators
    {
        private static string returnMessage;

        internal static string ValidateBlock_HeroCarousel(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);
            
            //Get list of stage elements (the big active one) and smaller dot elements (the small ones below).
            IList<IWebElement> largeStageElements = element.FindElement(By.ClassName("owl-stage")).FindElements(By.ClassName("owl-item"));
            IList<IWebElement> smallDotElements = element.FindElement(By.ClassName("owl-dots")).FindElements(By.ClassName("owl-dot"));

            //Remove clones
            largeStageElements = largeStageElements.Where(el => !el.GetAttribute("class").Contains("cloned")).ToList();
            

            //Actual validation below
            foreach (IWebElement small in smallDotElements)
            {
                
                int dotPosition = smallDotElements.IndexOf(small);
                small.Click();


                if (!largeStageElements.ElementAt(dotPosition).GetAttribute("class").Contains("active")
                    || !smallDotElements.ElementAt(dotPosition).GetAttribute("class").Contains("active"))
                {
                    return "Issue with Stage/Dot element at position" + dotPosition;
                }

            }

            //Everything works as intended
            return "";
        }

        internal static string ValidateBlock_Container(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);
            List<String> tabs = new List<String>
            {
                Browser.webDriver.WindowHandles.First()
            };

            //Check if this is a statscontainer
            if (element.FindElement(By.TagName("section")).GetAttribute("id") == "statscontainer")
            {

            }
            //Check if this is a Services container
            else if (element.FindElement(By.TagName("section")).GetAttribute("id") == "Services")
            {
                //Get all containers
                var containerContent = element.FindElements(By.ClassName("c-content-feature__block-link"));
                //For each container, check that there is a functioning link that can be clicked
                foreach (IWebElement e in containerContent)
                {
                    var elementLink = e.GetAttribute("href");


                    //Open block link in new tab
                    Actions action = new Actions(Browser.webDriver);
                    action.KeyDown(Keys.Control).MoveToElement(e).Click().Perform();
                    Thread.Sleep(1000); //Should probably not use this, but best i found

                    tabs.Add(Browser.webDriver.WindowHandles.Last());
                    Browser.SwitchToLastTab();
                    Webpage.WaitUntilPageIsLoaded();

                    if (Webpage.GetUrl() != elementLink)
                    {
                        return "Issue with link for block with title '" + e.GetAttribute("title") + "'. '" +
                                Webpage.GetUrl() + "' does not match expected value '" + elementLink + "'";
                    };
                    Browser.Close();
                    Browser.SwitchToFirstTab();

                }
            }
            else { return "Containerblock is of an unknown type, and does not have any associated test. "; }

            //Everything works as intended
            return "";
        }

        internal static string ValidateBlock_StandardPage(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);
            List<String> tabs = new List<String>
            {
                Browser.webDriver.WindowHandles.First()
            };

            //Get all containers. THere is Probablt just one and most of this code is redundent.
            var containerContent = element.FindElements(By.ClassName("c-content-feature__block-link"));
            //For each container, check that there is a functioning link that can be clicked
            foreach (IWebElement e in containerContent)
            {
                var elementLink = e.GetAttribute("href");


                //Open block link in new tab
                Actions action = new Actions(Browser.webDriver);
                action.KeyDown(Keys.Control).MoveToElement(e).Click().Perform();
                Thread.Sleep(1000); //Should probably not use this, but best i found

                tabs.Add(Browser.webDriver.WindowHandles.Last());
                Browser.SwitchToLastTab();
                Webpage.WaitUntilPageIsLoaded();

                if (Webpage.GetUrl() != elementLink)
                {
                    return "Issue with link for block with title '" + e.GetAttribute("title") + "'. '" +
                            Webpage.GetUrl() + "' does not match expected value '" + elementLink + "'";
                };
                Browser.Close();
                Browser.SwitchToFirstTab();

            }

            //Everything works as intended
            return "";
        }

        internal static string ValidateBlock_Testimonial(IWebElement element)
        {
            throw new NotImplementedException();
        }

        internal static string ValidateBlock_Imagefile(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);

            //Find actual image element
            var imgElement = element.FindElement(By.TagName("img"));

            //Check to see that image is displayed and have a source
            if(!imgElement.Displayed || String.IsNullOrEmpty(imgElement.GetAttribute("src")))
            {
                return "Problem with displaying image. ";
            }

            //Everything works as intended
            return "";
        }

        internal static string ValidateBlock_CTA(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);

            //US & CA forms are built different then all others, so those re routed to different test
            if (Webpage.GetUrl().Contains("/us/") || Webpage.GetUrl().Contains("/ca/"))
            {
                return ValidateBlock_CTA_USandCA(element);
            }


            string tempUrl = Webpage.GetUrl();
            string tempDataPageUrl = element.FindElement(By.TagName("article")).GetAttribute("data-page-url");
            
            //Verify that data-page-url is correct compared to site
            if (!tempUrl.Contains(tempDataPageUrl))
            {
                return "Form data-page-url " + tempDataPageUrl + " differs from site url + " + tempUrl + ". ";
            }

            try
            {
                returnMessage = "Issue with finding or clicking dropdown"; //return message if next step fails

                //Select first dropdown option
                var dropDown = element.FindElement(By.Id("F"));
                var options = dropDown.FindElements(By.TagName("option"));
                dropDown.Click();
                options[1].Click();
                //Ensure that the loading screen does not cover page
                bool clickBool = true;
                while (clickBool)
                {
                    clickBool = false;
                    try { dropDown.Click(); }
                    catch (InvalidOperationException) { Thread.Sleep(1000); clickBool = true; }
                }


                returnMessage = "Issue with finding inputFields - First Dropdown choice"; //return message if next step fails

                //Find inputFields
                var form1 = element.FindElement(By.ClassName("Form__MainBody"));
                var inputFields1 = form1.FindElements(By.ClassName("FormTextbox__Input"));
                var radioButtons1 = form1.FindElements(By.ClassName("FormChoice__FakeRadio"));
                var submitButton1 = form1.FindElement(By.TagName("button"));

                returnMessage = "Issue with filling out fields - First Dropdown choice"; //return message if next step fails

                //Fill out fields
                Browser.ScrollToElement(inputFields1[0]);
                inputFields1[0].Click();
                Webpage.AddTextInForm(inputFields1[0], "Random Firstname"); //Firstname
                Webpage.AddTextInForm(inputFields1[1], "Random Lastname"); //Lastname
                //Let email be empty to verify fieldvalidation
                //Webpage.AddTextInForm(inputFields[2], "fake@fake.no"); //Email (mandatory)
                Browser.ScrollToElement(inputFields1[3]);
                Webpage.AddTextInForm(inputFields1[3], "123456"); //Phone
                Webpage.AddTextInForm(inputFields1[4], "Fake Company"); //Company
                Browser.ScrollToElement(inputFields1[5]);
                Webpage.AddTextInForm(inputFields1[5], "This is a test. Please ignore"); //Question/comments

                returnMessage = "Issue with selecting radiobuttons - First Dropdown choice"; //return message if next step fails

                Browser.ScrollToElement(radioButtons1[0]);
                radioButtons1[0].Click(); //Answar via email
                radioButtons1[1].Click(); //Answar via phone

                returnMessage = "Issue with submitting- First Dropdown choice"; //return message if next step fails

                //Submit incomplete form (email missing)
                Browser.ScrollToElement(submitButton1);
                submitButton1.Submit();

                returnMessage = "Issue finding validationMessage for mandatory field - First Dropdown choice"; //return message if next step fails
                if (!DoesValidationErrorShowForForm(inputFields1[2]))
                {
                    return returnMessage;
                };

                // As some CTA blocks use three options (the third one being return), and the return form being very different,
                // I will not include that in these tests for now. However, as the "Technical Helpdesk" forms are sometimes third, and sometimes 
                // second, I need to take that into consideration.
                int formPosition;
                if (options.Count == 3) { formPosition = 2; }
                else if (options[2].Text.Contains("Support") || options[2].Text.Contains("Service")) { formPosition = 2; }
                else { formPosition = 3; }      

                dropDown.Click();
                options[formPosition].Click();
                //Ensure that the loading screen does not cover the form
                clickBool = true;
                while (clickBool)
                {
                    clickBool = false;
                    try { dropDown.Click(); }
                    catch (InvalidOperationException) { Thread.Sleep(1000); clickBool = true; }
                }
                //Find inputFields
                var form2 = element.FindElement(By.ClassName("Form__MainBody"));
                var inputFields2 = form2.FindElements(By.ClassName("FormTextbox__Input"));
                var submitButton2 = form2.FindElement(By.TagName("button"));

                returnMessage = "Issue with filling out fields - Second Dropdown choice"; //return message if next step fails

                //Fill out fields
                Browser.ScrollToElement(inputFields2[0]);
                inputFields2[0].Click();
                Webpage.AddTextInForm(inputFields2[0], "Random Firstname"); //Firstname
                Webpage.AddTextInForm(inputFields2[1], "Random Lastname"); //Lastname
                //Let email be empty to verify fieldvalidation
                //Webpage.AddTextInForm(inputFields[2], "fake@fake.no"); //Email (mandatory)
                Browser.ScrollToElement(inputFields2[3]);
                Webpage.AddTextInForm(inputFields2[3], "Fake Company"); //Company
                Browser.ScrollToElement(inputFields2[4]);
                Webpage.AddTextInForm(inputFields2[4], "No Product"); //Product
                Webpage.AddTextInForm(inputFields2[5], "This is a test. Please ignore"); //Question/comments

                returnMessage = "Issue with submitting- Second Dropdown choice"; //return message if next step fails

                //Submit incomplete form (email missing)
                Browser.ScrollToElement(submitButton2);
                submitButton2.Submit();

                returnMessage = "Issue finding validationMessage for mandatory field - Second Dropdown choice"; //return message if next step fails
                if (!DoesValidationErrorShowForForm(inputFields2[2]))
                {
                    return returnMessage;
                };

            } catch(Exception ex) { return returnMessage + " Exception details:" + ex.ToString(); }

            //Everything works as intended
            return "";
        }

        internal static string ValidateBlock_SimpleEditorial(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);

            //Check that header is displayed
            if (!element.FindElement(By.ClassName("c-editorial__heading")).Displayed)
            {
                return "Problem finding editorial header. ";
            }

            //Check that body is displayed
            if (!element.FindElement(By.ClassName("c-editorial__body")).Displayed)
            {
                return "Problem finding editorial body. ";
            }



            //Everything works as intended
            return "";
        }

        internal static string ValidateBlock_Wrapping(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);
            List<String> tabs = new List<String>
            {
                Browser.webDriver.WindowHandles.First()
            };

            //Get all wrapped blocks
            var wrappedContent = element.FindElements(By.ClassName("c-content-feature__block-link"));

            //For each container, check that there is a functioning link that can be clicked
            foreach (IWebElement e in wrappedContent)
            {
                //Check for image
                try
                {
                    if (e.FindElement(By.TagName("img")).GetAttribute("src") == "") { throw new NoSuchElementException(); };
                }
                catch (Exception)
                {
                    return "Wrapped block is missing or have invalid image. ";
                }

                var elementLink = e.GetAttribute("href");
                
                //Open block link in new tab
                Actions action = new Actions(Browser.webDriver);
                action.KeyDown(Keys.Control).MoveToElement(e).Click().Perform();
                Thread.Sleep(1000); //Should probably not use this, but best i found

                tabs.Add(Browser.webDriver.WindowHandles.Last());
                Browser.SwitchToLastTab();
                Webpage.WaitUntilPageIsLoaded();

                if (Webpage.GetUrl() != elementLink)
                {
                    return "Issue with link for block with title '" + e.GetAttribute("title") + "'. '" +
                            Webpage.GetUrl() + "' does not match expected value '" + elementLink + "'";
                };
                Browser.Close();
                Browser.SwitchToFirstTab();

            }

            //Everything works as intended
            return "";
        }

        private static string ValidateBlock_CTA_USandCA(IWebElement element)
        {
            //Not implemented for now
            return "";
            throw new NotImplementedException();
        }

        internal static string ValidateBlock_WhatsNewContainer(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);
            List<String> tabs = new List<String>
            {
                Browser.webDriver.WindowHandles.First()
            };

            //get list of blocks
            var newContentList = element.FindElements(By.ClassName("whatsnew"));

            //For each whatsNewBlock, check that there is a functioning link that can be clicked
            foreach (IWebElement e in newContentList)
            {
                var elementLink = e.FindElement(By.TagName("a")).GetAttribute("href");

                //Open block link in new tab
                Actions action = new Actions(Browser.webDriver);
                action.KeyDown(Keys.Control).MoveToElement(e).Click().Perform();
                Thread.Sleep(1000); //Should probably not use this, but best i found

                Browser.SwitchToLastTab();
                Webpage.WaitUntilPageIsLoaded();

                if (Webpage.GetUrl() != elementLink)
                {
                    return "Issue with link for block with title '" + Browser.Title + "'. '" +
                            Webpage.GetUrl() + "' does not match expected value '" + elementLink + "'";
                };
                Browser.Close();
                Browser.SwitchToFirstTab();

            }

            //Everything works as intended
            return "";
        }

        internal static string ValidateBlock_FeaturedContent(IWebElement element)
        {
            Browser.webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Browser.ScrollToElement(element);
            List<String> tabs = new List<String>
            {
                Browser.webDriver.WindowHandles.First()
            };

            //Check if block have image or video attached
            var imgWrapElement = element.FindElement(By.ClassName("c-featured-content__img-wrap"));
            var ImgOrVidElement = imgWrapElement.FindElement(By.XPath(".//*"));
            bool IsElementImgBool;
            string VidHref = "";


            if (ImgOrVidElement.TagName == "img")
            {
                IsElementImgBool = true;
            }
            else if (ImgOrVidElement.TagName == "article")
            {
                IsElementImgBool = false;
                VidHref = ImgOrVidElement.FindElement(By.TagName("a")).GetAttribute("href");
            }
            else { return "Not able to finde related image or video. "; }

            //Verify that video is working
            if(!IsElementImgBool)
            {
                CreateAction().KeyDown(Keys.Control).MoveToElement(ImgOrVidElement).Click().Perform();
                Thread.Sleep(1000); //Should probably not use this, but best i found
                tabs.Add(Browser.webDriver.WindowHandles.Last());

                Browser.SwitchToLastTab();
                Webpage.WaitUntilPageIsLoaded();

                if (Webpage.GetUrl() != VidHref)
                {
                    return "Issue with link for block with title '" + element.FindElement(By.ClassName("p")).Text + "'. '" +
                            Webpage.GetUrl() + "' does not match expected value '" + VidHref + "'";
                };
                Browser.Close();
                Browser.SwitchToFirstTab();
            }


            //Verify that the block have an image. Will fail if element does not exist (which is also ok)
            if (IsElementImgBool && ImgOrVidElement.GetAttribute("src") == "")
                {
                    return "Could not find image related to FeturedContentBlock. ";
                }

            //Verify if the block have a button, and if the block link is valid
            if (Webpage.DoesElementExist(element, By.TagName("a")))
            {
                var blockButton = element.FindElement(By.TagName("a"));
                var elementLink = blockButton.GetAttribute("href");

                //Open block link in new tab
                Actions action = new Actions(Browser.webDriver);
                action.KeyDown(Keys.Control).MoveToElement(blockButton).Click().Perform();
                Thread.Sleep(1000); //Should probably not use this, but best i found
                tabs.Add(Browser.webDriver.WindowHandles.Last());

                Browser.SwitchToLastTab();
                Webpage.WaitUntilPageIsLoaded();

                if (Webpage.GetUrl() != elementLink)
                {
                    return "Issue with link for block with title '" + element.FindElement(By.ClassName("p")).Text + "'. '" +
                            Webpage.GetUrl() + "' does not match expected value '" + elementLink + "'";
                };
                Browser.Close();
                Browser.SwitchToFirstTab();
            }

            //Everything works as intended
            return "";
        }

        


        //Helping functions
        private static bool DoesValidationErrorShowForForm(IWebElement inputFormElement)
        {
            var ValidationErrorElement = inputFormElement.FindElement(By.XPath("..")).FindElement(By.TagName("span"));
            if (ValidationErrorElement.Displayed && ValidationErrorElement.Text != "") { return true; }
            return false;
        }

        private static Actions CreateAction()
        {
            return new Actions(Browser.webDriver);
        }
    }
}
