﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFramework.SupportClasses
{
    //Class used to simplify use of XPath selection of custom values. See examples for individual functions.
    class Selectors
    {

        //Example of use: FindElement(Selectors.SelectorByAttributeValue("data-power","5"))
        public static By SelectorByAttributeValue(string p_strAttributeName, string p_strAttributeValue)
        {
            return (By.XPath(String.Format("//*[@{0} = '{1}']",
                                           p_strAttributeName,
                                           p_strAttributeValue)));
        }

        //Example of use: FindElement(Selectors.SelectorByTagAndAttributeValue("div", "data-power", "5"))
        public static By SelectorByTagAndAttributeValue(string p_strTagName, string p_strAttributeName, string p_strAttributeValue)
        {
            return (By.XPath(String.Format("//{0}[@{1} = '{2}']",
                                           p_strTagName,
                                           p_strAttributeName,
                                           p_strAttributeValue)));
        }


        //Example of use: FindElement(Selectors.SelectorByContainsText("Login"))
        public static By SelectorByContainsText(string p_strText)
        {
            return (By.XPath("//*[contains(text(), '" + p_strText + "')]"));
        }
    }

}
