﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;
using TestFramework.SupportClasses;

namespace Tests.SimpleTests
{
    [TestClass]
    public class VerifyLaerdalSites : BaseSimpleTest
    {
        [TestMethod]
        public void Can_Go_To_Laerdal_Webpage()
        {
            Webpage.GoTo(siteUrl);
            Assert.IsTrue(Webpage.IsAtDomain(siteUrl));
        }

        [TestMethod]
        public void Test_used_for_debugging()
        {
            Webpage.GoTo(siteUrl);

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks(); 
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }
    }

}
