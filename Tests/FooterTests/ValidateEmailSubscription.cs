﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace Tests.SimpleTests
{
    [TestClass]
    public class ValidateEmailSubscription : BaseSimpleTest
    {

        [TestMethod]
        public void Subscribe_to_newsletter_from_US_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("us");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.UpdateNewsletterProfile();
            Assert.IsTrue(Webpage.Footer.ReceiveSignupConfirmation());

        }

        [TestMethod]
        public void Reset_newsletter_subscription_form_US_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("us");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.ResetForm();
            Assert.IsTrue(Webpage.Footer.IsFormEmpty());
        }

        [TestMethod]
        public void Unsubscribe_to_newsletter_from_US_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("us");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.Unsubscribe();
            Webpage.Footer.UpdateNewsletterProfile();
            Assert.IsTrue(Webpage.Footer.ReceiveSignupConfirmation());
        }

        [TestMethod]
        public void Subscribe_to_newsletter_from_CA_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("ca");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.UpdateNewsletterProfile();
            Assert.IsTrue(Webpage.Footer.ReceiveSignupConfirmation());
        }

        [TestMethod]
        public void Reset_newsletter_subscription_form_CA_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("ca");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.ResetForm();
            Assert.IsTrue(Webpage.Footer.IsFormEmpty());
        }

        [TestMethod]
        public void Unsubscribe_to_newsletter_from_CA_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("ca");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.Unsubscribe();
            Webpage.Footer.UpdateNewsletterProfile();
            Assert.IsTrue(Webpage.Footer.ReceiveSignupConfirmation());
        }

        [TestMethod]
        public void Subscribe_to_newsletter_from_BR_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("br");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.UpdateNewsletterProfile();
            Assert.IsTrue(Webpage.Footer.ReceiveSignupConfirmation());
        }

        [TestMethod]
        public void Reset_newsletter_subscription_form_BR_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("br");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.ResetForm();
            Assert.IsTrue(Webpage.Footer.IsFormEmpty());
        }

        [TestMethod]
        public void Unsubscribe_to_newsletter_from_BR_site()
        {
            Browser.Goto(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("br");
            Webpage.Footer.InitiateSignupToNewsletter();
            Webpage.Footer.FillInNewsletterInformation();
            Webpage.Footer.Unsubscribe();
            Webpage.Footer.UpdateNewsletterProfile();
            Assert.IsTrue(Webpage.Footer.ReceiveSignupConfirmation());
        }
    }
}
