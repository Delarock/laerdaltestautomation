﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace Tests.SimpleTests
{
    [TestClass]
    public class HeadersInFooter : BaseHeaderTest
    {
        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_NO()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("no");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList){
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }
            
        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_SE()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("se");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_DK()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("dk");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_US()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("us");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_GB()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("gb");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_CA()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("ca");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_AU()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("au");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_CN()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("cn");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_HK()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("hk");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_IN()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("in");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_JP()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("jp");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_KR()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("kr");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_MY()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("my");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_SG()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("sg");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_NZ()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("nz");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_BENL()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("be-nl");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_BEFR()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("be-fr");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_DE()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("de");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_FR()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("fr");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_NL()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("nl");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_AT()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("at");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_CH()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("ch");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_FI()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("fi");
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }

        [TestMethod]
        public void Verify_that_headerHeaders_are_found_in_footer_INT()
        {


            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_IntPageViaFooter();
            List<string> headerHeaderList = Webpage.Header.GetHeaderHeaders();
            List<string> footerHeaderList = Webpage.Footer.GetFooterHeaders();
            headerHeaderList.Sort();
            footerHeaderList.Sort();

            foreach (var h in headerHeaderList)
            {
                Assert.AreEqual(h, footerHeaderList.ElementAt(headerHeaderList.IndexOf(h)));
            }

        }
    }
}
