﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestFramework;
using TestFramework.SupportClasses;

namespace Tests.HeaderTests
{
    [TestClass]
    public class HeaderNavigation : BaseHeaderTest
    {
        //This tests check that the target for a link in the header have a tittel that relates to the link-txt. 

        [TestMethod]
        public void Can_navigate_from_main_header()
        {
            Webpage.GoTo(siteUrl);
            Webpage.ClearAllCookies();
            List<HeaderModule> HeaderNavigationList = Webpage.Header.GetHeaderLinks();

            foreach (HeaderModule h in HeaderNavigationList)
            {
                Browser.Goto(h.headerModuleLink);
                Assert.IsTrue(Browser.Title == h.headerModuleText, "Failed on test for link " + h.headerModuleText.Trim() + " and path " + h.headerModuleLink);
            }

        }

        [TestMethod]
        public void ShoppingBasket_hidden_on_IntPage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_IntPageViaFooter();
            Assert.IsFalse(Webpage.Header.FindCart());
        }

        [TestMethod]
        public void ShoppingBasket_hidden_on_LatinPage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("la");
            Assert.IsFalse(Webpage.Header.FindCart());
        }

        [TestMethod]
        public void ShoppingBasket_visible_on_NoPage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("no");
            Assert.IsTrue(Webpage.Header.FindCart());
        }

        [TestMethod]
        public void ShoppingBasket_visible_on_UsPage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("us");
            Assert.IsTrue(Webpage.Header.FindCart());
        }

        [TestMethod]
        public void ShoppingBasket_visible_on_GbPage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("gb");
            Assert.IsTrue(Webpage.Header.FindCart());
        }

        [TestMethod]
        public void ShoppingBasket_hidden_on_CnPage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("cn");
            Assert.IsFalse(Webpage.Header.FindCart());
        }

    }
}
