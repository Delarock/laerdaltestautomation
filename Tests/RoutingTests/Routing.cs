﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestFramework;
using System.Collections.Generic;

namespace Tests
{

    [TestClass]
    public class Routing : BaseRoutingTest
    {
        //These tests verify that the routing rules are in effect, and that geo-routing and cookies are used as needed

        [TestMethod]
        public void IntSite_withoutCookie_withoutRefferer()
        {
            string targetPage = siteUrl;
            string expectedPage = siteUrl + "no/";
            string actualPage = "";

            Webpage.GoTo(targetPage);
            Webpage.ClearAllCookies();
            Webpage.GoTo(targetPage);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void IntSite_withoutCookie_internalRefferer()
        {
            string targetPage = siteUrl + "in/";
            string expectedPage = siteUrl +"no/";
            string actualPage = "";

            Webpage.GoTo(targetPage);
            Webpage.ClearAllCookies();
            Webpage.Footer.GoTo_IntPageViaFooter();
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void IntSite_withoutCookie_externalRefferer()
        {
            string targetPage = "https://www.google.no/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjn0JqTnuTYAhXQFOwKHdoPCiMQFggvMAA&url=" + siteUrl;
            string expectedPage = siteUrl + "no/";
            string actualPage = "";

            Webpage.GoTo("Http:\\www.google.com");
            Webpage.ClearAllCookies();
            Webpage.GoTo(targetPage);
            Webpage.ClickLink(siteUrl);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void IntSite_withCookieX_internalRefferer() 
        {
            string targetPage = siteUrl + "us/";
            string expectedPage = siteUrl;
            string actualPage = "";

            Webpage.ClearAllCookies();
            Webpage.GoTo(targetPage);
            Webpage.Footer.GoTo_IntPageViaFooter();
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withCookieX_externalRefferer()
        {
            string targetPage = "https://www.google.no/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiF2vabpeTYAhXFliwKHdJFChMQFggyMAA&url=" + siteUrl + "dk%2F&usg=AOvVaw2Lqkh_iLX5lngw-uqh3H2A";
            string expectedPage = siteUrl + "in/";
            string actualPage = "";


            Webpage.GoTo(siteUrl);
            Webpage.ClearAllCookies();
            Webpage.GoTo(expectedPage);
            Webpage.GoTo("Http:\\www.google.com");
            Webpage.GoTo(targetPage);
            Webpage.ClickLink(siteUrl + "dk/"); //Sometimes google adds a step, if sending you to a different site. 
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void IntSite_withCookieX_withoutRefferer()
        {
            string targetPage = siteUrl;
            string expectedPage = siteUrl + "se/";
            string actualPage = "";


            Webpage.GoTo(siteUrl);
            Webpage.ClearAllCookies();
            Webpage.GoTo(expectedPage);
            Webpage.GoTo(targetPage);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void IntSite_withCookieX_externalRefferer()
        {
            string targetPage = "https://www.google.no/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjn0JqTnuTYAhXQFOwKHdoPCiMQFggvMAA&url=" + siteUrl + "&g=AOvVaw2q_bsheJ1LerE_7TSr5Y76";
            string expectedPage = siteUrl + "au/";
            string actualPage = "";


            Webpage.GoTo(siteUrl);
            Webpage.ClearAllCookies();
            Webpage.GoTo(expectedPage);
            Webpage.GoTo("Http:\\www.google.com");
            Webpage.GoTo(targetPage);
            Webpage.ClickLink(siteUrl);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withoutCookie_withoutRefferer()
        {
            string expectedPage = siteUrl + "se/";
            string actualPage = "";


            Webpage.GoTo("http://vg.no");
            Webpage.ClearAllCookies();
            Webpage.GoTo(expectedPage);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withoutCookie_internalRefferer()
        {
            string targetPage = siteUrl +"no/";
            string expectedPage = siteUrl + "gb/";
            string actualPage = "";

            Webpage.ClearAllCookies();
            Webpage.GoTo(targetPage);
            Webpage.Footer.GoTo_CountryPageViaFooter("gb");
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withoutCookie_externalRefferer()
        {
            string targetPage = "https://www.google.no/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiF2vabpeTYAhXFliwKHdJFChMQFggyMAA&url=" + siteUrl +"nz%2F&usg=AOvVaw2Lqkh_iLX5lngw-uqh3H2A";
            string expectedPage = siteUrl + "nz/";
            string actualPage = "";


            Webpage.GoTo(siteUrl);
            Webpage.ClearAllCookies();
            Webpage.GoTo("Http:\\www.google.com");
            Webpage.ClearAllCookies();
            Webpage.GoTo(targetPage);
            Webpage.ClickLink(expectedPage);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withCookieX_withoutRefferer()
        {
            string targetPage = siteUrl + "se/";
            string expectedPage = siteUrl + "de/";
            string actualPage = "";


            Webpage.ClearAllCookies();
            Webpage.GoTo(siteUrl);
            Webpage.ClearAllCookies();
            Webpage.GoTo(targetPage);
            Webpage.GoTo(expectedPage);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withCookieX_internalRefferer()
        {
            string targetPage =  siteUrl + "no/";
            string expectedPage = siteUrl + "br/";
            string actualPage = "";

            Webpage.ClearAllCookies();
            Webpage.GoTo(targetPage);
            Webpage.Footer.GoTo_CountryPageViaFooter("br");
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withCookieInt_withoutRefferer()
        {
            string expectedPage = siteUrl + "se/";
            string actualPage = "";

            Webpage.ClearAllCookies();
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_IntPageViaFooter();
            Webpage.GoTo(expectedPage);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withCookieInt_internalRefferer()
        {
            string targetPage = siteUrl;
            string expectedPage = siteUrl + "us/";
            string actualPage = "";

            Webpage.GoTo(targetPage);
            Webpage.ClearAllCookies();
            Webpage.GoTo(targetPage);
            Webpage.Footer.GoTo_CountryPageViaFooter("us");
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

        [TestMethod]
        public void YSite_withCookieInt_externalRefferer()
        {
            string targetPage = "https://www.google.no/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiF2vabpeTYAhXFliwKHdJFChMQFggyMAA&url=" + siteUrl + "ca%2F&usg=AOvVaw2Lqkh_iLX5lngw-uqh3H2A";
            string expectedPage = siteUrl + "ca/";
            string actualPage = "";


            Webpage.ClearAllCookies();
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_IntPageViaFooter();
            //Pages.HomePage.GoTo("Http:\\www.google.com");
            Webpage.GoTo(targetPage);
            Webpage.ClickLink(expectedPage);
            actualPage = Webpage.GetUrl();
            Assert.AreEqual(expectedPage, actualPage, "The expected site " + expectedPage + " is not equal to actual page " + actualPage);

        }

    }

        
    
}
