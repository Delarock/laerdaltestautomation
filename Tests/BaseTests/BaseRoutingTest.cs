﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestFramework;

namespace Tests
{
    public abstract class BaseRoutingTest : _BaseTestConfig
    {
        [TestInitialize]
        public void ClassInitialize()
        {
            Webpage.OpenBrowser();
            Webpage.ClearAllCookies();

        }

        [TestCleanup]
        public void CleanUp()
        {
            Webpage.ClearAllCookies();
            Webpage.Quit();
        }
    }
}
