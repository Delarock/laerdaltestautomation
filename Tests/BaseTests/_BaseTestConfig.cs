﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

// This is the base "test class" that all the other test classes, incl. the other base tests, inherit.
// Mostly it is used to specify the site or environment to test, and also to ensure that initialization and
// teardown is being done consistently for each test. 
// These functions here should be overridden when needed.

namespace Tests
{
    [TestClass]
    public abstract class _BaseTestConfig
    {
        //Change this to the domain/environment you want to test. Remember to include "/" at end
        public static String siteUrl = "https://p2uat.laerdal.com/";
        //The browser to use, in lowercase
        public static String browserType = "firefox"; 

    }
}
