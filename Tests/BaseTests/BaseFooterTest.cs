﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace Tests
{
    public abstract class BaseFooterTest : _BaseTestConfig
    {
        [TestInitialize]
        public virtual void ClassInitialize()
        {
            Webpage.OpenBrowser(browserType);

        }

        [TestCleanup]
        public virtual void CleanUp()
        {
            Webpage.Quit();
        }
    }
}
