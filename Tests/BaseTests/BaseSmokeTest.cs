﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace Tests
{
    public abstract class BaseSmokeTest : _BaseTestConfig
    {
        [TestInitialize]
        public virtual void TestInitialize()
        {
            Webpage.OpenBrowser(browserType);
            Webpage.ClearAllCookies();

        }

        [TestCleanup]
        public virtual void CleanUp()
        {
            Webpage.Quit();
        }
    }
}
