﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace Tests.SmokeTests.SignIn
{
    namespace Tests.SmokeTests.Signin
    {
        [TestClass]
        public class Pricing : BaseSmokeTest
        {
            [TestCategory("Smoketest"), TestMethod]
            public void Log_in()
            {
                Browser.Goto(siteUrl);
                Webpage.Header.ClickMyAccount();
                Webpage.Main.SignInWith("TestUserLM-1@outlook.com", "LMTestUser1"); //Should probably use a getTestUser[].Email()/.Pass(); function
                Assert.IsTrue(Webpage.GetUrl().EndsWith("my-account/"));
            }

            [TestCategory("Smoketest"), TestMethod]
            public void Log_out()
            {
                Browser.Goto(siteUrl);
                Webpage.Header.ClickMyAccount();
                if (Webpage.GetUrl().Contains("signin"))
                {
                    Webpage.Main.SignInWith("TestUserLM-1@outlook.com", "LMTestUser1"); //Should probably use a getTestUser[].Email()/.Pass(); function
                }
                Webpage.ClickLink("Sign Out");
                Assert.IsTrue(Webpage.GetUrl().Equals(siteUrl));
            }


        }
    }
}
