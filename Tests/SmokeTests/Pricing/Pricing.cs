﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace Tests.SmokeTests.Pricing
{
    [TestClass]
    public class Pricing : BaseSmokeTest
    {
        [TestCategory("Smoketest"), TestMethod]
        public void Verify_that_available_variants_have_prices_and_status()
        {
            Browser.Goto(siteUrl);
            Webpage.Header.GoToHeaderLevelThree(4, 0, 0); //When all products links go to the catalog, this should probably be randomized.
            Webpage.Main.ClickProduct("SkillGuide"); //This should also be randomized when all Main Products have variants //SkillGuide Resusci Anne QCPR
            Webpage.Main.ClickShopButton();
            Assert.IsTrue(Webpage.Main.VerifyThatVariantsHavePrices()); //If randomized, a message should include where the test fails
        }


    }
}
