﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace Tests.SmokeTests.Search
{
    [TestClass]
    public class Search : BaseSmokeTest
    {
        [TestCategory("Smoketest"), TestMethod]
        public void Verify_That_Search_Icon_Exists()
        {
            Browser.Goto(siteUrl);
            Assert.IsTrue(Webpage.Header.FindSearch());
        }

        [TestCategory("Smoketest"), TestMethod]
        public void Search_On_Product_Name()
        {
            Browser.Goto(siteUrl);
            Webpage.Header.SearchFor("Anne");
            Assert.AreEqual(Webpage.GetUrl(), "https://p2uat.laerdal.com/search.aspx?q=Anne");
        }
    }
}
