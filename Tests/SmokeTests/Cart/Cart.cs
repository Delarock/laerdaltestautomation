﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace Tests.SmokeTests.Cart
{
    [TestClass]
    public class Cart : BaseSmokeTest
    {
        [TestCategory("Smoketest"), TestMethod]
        public void Verify_that_cart_icon_exist()
        {
            Browser.Goto(siteUrl);
            Assert.IsTrue(Webpage.Header.FindCart());
        }

        [TestCategory("Smoketest"), TestMethod]
        public void Open_miniCart_with_empty_cart()
        {
            Browser.Goto(siteUrl);
            Webpage.Header.OpenMiniCart();
            Assert.IsTrue(!Webpage.IsMiniCartDisplayed());
        }

        [TestCategory("Smoketest"), TestMethod]
        public void Open_miniCart_with_items_in_cart()
        {
            Browser.Goto(siteUrl);
            if (Webpage.Header.GetCartItemCount() == 0)
            {
                Webpage.Header.GoToHeaderLevelThree(4, 0, 0);
                Webpage.Main.ClickProduct("Resusci Anne QCPR");
                Webpage.Main.ClickShopButton();
                Webpage.Main.AddFirstAvailableItemToCart();
            }
            Webpage.Header.OpenMiniCart();
            Assert.IsTrue(Webpage.IsMiniCartDisplayed());
        }

        [TestCategory("Smoketest"), TestMethod]
        public void Open_cart()
        {
            Browser.Goto(siteUrl);
            Webpage.Header.OpenCart();
            Assert.AreEqual(siteUrl + "cart/", Webpage.GetUrl());
        }

        [TestCategory("Smoketest"), TestMethod]
        public void Manually_add_item_to_cart()
        {
            Browser.Goto(siteUrl);

            //Go to Simulation & Training -> Resusication Training
            var initialItemCount = Webpage.Header.GetCartItemCount();
            Webpage.Header.GoToHeaderLevelThree(4, 0, 0);
            Webpage.Main.ClickProduct("Resusci Anne QCPR");
            Webpage.Main.ClickShopButton();
            Webpage.Main.AddFirstAvailableItemToCart();
            var newItemCount = Webpage.Header.GetCartItemCount();

            Assert.IsTrue(initialItemCount < newItemCount);
        }

        [TestCategory("Smoketest"), TestMethod]
        public void Change_item_quantity_in_minicart()
        {
            Browser.Goto(siteUrl);
            var initialItemCount = Webpage.Header.GetCartItemCount();
            if (initialItemCount == 0)
            {
                Webpage.Header.GoToHeaderLevelThree(4, 0, 0);
                Webpage.Main.ClickProduct("Resusci Anne QCPR");
                Webpage.Main.ClickShopButton();
                Webpage.Main.AddFirstAvailableItemToCart();
                initialItemCount = Webpage.Header.GetCartItemCount();
            }
            else
            {
                Webpage.Header.OpenMiniCart();
            }
            Webpage.ClickLink("Remove");
            var newItemCount = Webpage.Header.GetCartItemCount();

            Assert.IsTrue(initialItemCount > newItemCount);



        }

        [TestCategory("Smoketest"), TestMethod]
        public void Change_item_quantity_in_cart()
        {
            Browser.Goto(siteUrl);
            var initialItemCount = Webpage.Header.GetCartItemCount();
            if (initialItemCount == 0)
            {
                Webpage.Header.GoToHeaderLevelThree(4, 0, 0);
                Webpage.Main.ClickProduct("Resusci Anne QCPR");
                Webpage.Main.ClickShopButton();
                Webpage.Main.AddFirstAvailableItemToCart();
                initialItemCount = Webpage.Header.GetCartItemCount();
            }
            else
            {
                Webpage.Header.OpenMiniCart();
            }
            Webpage.ClickLink("View cart & checkout");
            Webpage.ClickElement("ClassName", "c-line-items__qty-minus");

            var newItemCount = Webpage.Header.GetCartItemCount();

            Assert.IsTrue(initialItemCount > newItemCount);
        }

        [TestCategory("Smoketest"), TestMethod]
        public void Clear_Cart()
        {
            Browser.Goto(siteUrl);
            var initialItemCount = Webpage.Header.GetCartItemCount();
            if (initialItemCount == 0)
            {
                Webpage.Header.GoToHeaderLevelThree(4, 0, 0);
                Webpage.Main.ClickProduct("Resusci Anne QCPR");
                Webpage.Main.ClickShopButton();
                Webpage.Main.AddFirstAvailableItemToCart();
                Webpage.Main.AddFirstAvailableItemToCart();
                initialItemCount = Webpage.Header.GetCartItemCount();
            }
            else
            {
                Webpage.Header.OpenMiniCart();
            }

            Webpage.Main.EmptyCart();

            Assert.IsTrue(Webpage.Header.GetCartItemCount() == 0);
        }
        [TestCategory("Smoketest"), TestMethod]
        public void Add_items_to_cart_using_Easy_Order_Entry()
        {
            Browser.Goto(siteUrl);

            if (Webpage.Header.GetCartItemCount() > 0)
            {
                Webpage.Main.EmptyCart();
            }

            Webpage.Header.OpenCart();
            Webpage.Main.AddItemViaEasyOrderEntry("173-00150", "2"); //Should add random code and quantity here

            Assert.IsTrue(Webpage.Header.GetCartItemCount() > 0);
        }

    }
}
