﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;
using TestFramework.SupportClasses;

namespace Tests.MainTests
{
    [TestClass]
    public class VerifyNorwayLevelTwo : BaseMainTest
    {
        [TestMethod]
        public void VerifyNorwaySite_FirstHeader_FirstSubHeader()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("no");
            Webpage.Header.GoToHeaderLevelTwo(0,0);
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

    }
}
