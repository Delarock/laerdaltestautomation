﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;
using TestFramework.SupportClasses;

namespace Tests.MainTests
{
    [TestClass]
    public class VerifyNorwayLevelOne : BaseMainTest
    {
        [TestMethod]
        public void VerifyNorwaySite_FirstHeaderPage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("no");
            Webpage.Footer.GoToFooterHeaderLevelOne(2); //Footer order is different than header
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void VerifyNorwaySite_FifthHeaderPage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("no");
            Webpage.Footer.GoToFooterHeaderLevelOne(0); //Footer order is different than header
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }


    }
}
