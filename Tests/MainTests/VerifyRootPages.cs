﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;
using TestFramework.SupportClasses;

namespace Tests.MainTests
{
    [TestClass]
    public class VerifyRootPages : BaseMainTest
    {
        [TestMethod]
        public void Verify_NO_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("no");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_INT_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_IntPageViaFooter();

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_US_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("us");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_CA_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("ca");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_LA_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("la");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_BR_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("br");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_AU_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("au");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_CN_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("cn");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_HK_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("hk");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_IN_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("in");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_JP_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("jp");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_KR_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("kr");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_MY_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("my");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_NZ_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("nz");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }

        [TestMethod]
        public void Verify_SG_rootpage()
        {
            Webpage.GoTo(siteUrl);
            Webpage.Footer.GoTo_CountryPageViaFooter("sg");

            //Return a bool indicating if tests fail, and string detailing on what block.
            BlockResultAndString result = Webpage.Main.VerifyBlocks();
            Assert.IsTrue(result.testStatus, result.blockInfo);
        }
    }

}